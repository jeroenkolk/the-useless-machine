package com.mycms.tum.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.PrintWriter;

@Slf4j
@Controller
public class TableEchoController {

    @GetMapping("/")
    public String test() {
        return "test";
    }

    @PostMapping("/")
    @ResponseBody
    public void convertImageToFile(@RequestParam final MultipartFile file, final HttpServletResponse response) throws IOException {
        final BufferedImage image = ImageIO.read(file.getInputStream());
        final PrintWriter writer = response.getWriter();

        writer.print("<html><head></head><body>");
        writer.print("<form method=\"post\" action=\"/\" enctype=\"multipart/form-data\"><input type=\"file\" name=\"file\"/><button type=\"submit\">Submit</button></form>");
        if (image == null) {
            writer.write("<h1>Image not supported</h1>");
        } else {
            int pixels = image.getWidth() * image.getHeight();
            writer.print("<h2>Printing back " + pixels + " pixels!");
            writer.print("<table style='border-collapse: collapse;'>");
            for (int y = 0; y < image.getHeight(); y++) {
                writer.print("<tr>");
                for (int x = 0; x < image.getWidth(); x++) {
                    final Color color = new Color(image.getRGB(x, y));
                    final String hex = String.format("#%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue());
                    writer.print("<td style='background-color: " + hex + ";'></td>");
                }
                writer.print("</tr>");
            }
            writer.print("</table>");
            writer.print("</body></html>");
            writer.flush();
        }

    }
}
