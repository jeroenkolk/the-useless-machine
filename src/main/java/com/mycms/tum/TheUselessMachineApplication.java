package com.mycms.tum;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TheUselessMachineApplication {

    public static void main(String[] args) {
        SpringApplication.run(TheUselessMachineApplication.class, args);
    }

}
