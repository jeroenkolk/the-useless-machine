# The Useless Machine

A simple project that just has some fun webpages in it.

## What's included?
* [x] Image upload to html `<table>` converter
* [ ] The useless machine itself

## How does it work?

Just clone the project and run
```console
$ sh mvnw spring-boot:run
```
and open an browser window and navigate to [http://localhost:8080](http://localhost:8080)
